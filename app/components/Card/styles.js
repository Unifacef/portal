export default {
  '.list': {
    marginTop: 8,
    paddingHorizontal: 8,
  },
  '.margedHorizontal': {
    marginHorizontal: 16,
  },
  '.padderHorizontal': {
    paddingHorizontal: 15,
  },
  '.padderAll': {
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.16,
  shadowRadius: 3.84,
  elevation: 3,
  borderRadius: 12,
  borderColor: 'transparent',
};
