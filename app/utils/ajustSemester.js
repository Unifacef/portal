import { toCapitalize } from './';

export default (semester) => {
  const numbers = semester.slice(0, 3);
  const stringSemester = semester.slice(4);
  return `${numbers.replace('º', '°')} ${toCapitalize(stringSemester)}`;
}