import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import { View } from 'native-base';

import { connectStyle } from '../../utils';
import styles from './styles';

class RadioButton extends PureComponent {
  render() {
    const { checked, onPress, children } = this.props;

    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View>
          <View
            style={{
              borderWidth: checked ? 5 : 0,
              borderColor: checked ? '#454F63' : '#d6d6d6',
            }}
          >
            <View
              style={{
                backgroundColor: checked ? '#454F63' : '#d6d6d6',
              }}
            />
          </View>
          { children }
        </View>
      </TouchableOpacity>
    );
  }
}

export default connectStyle('Portal.RadioButton', styles, RadioButton);
