import React, { PureComponent } from 'react';
import { Button as NBButton, Text } from 'native-base';

import styles from './styles';
import { connectStyle, computeProps } from '../../utils';

class Button extends PureComponent {
  render() {
    const { style } = computeProps(this.props);
    const { color, placeholder, onPress } = this.props;

    return (
      <NBButton
        style={[style, {
          backgroundColor: color || '#454F63',
        }]}
        {...this.props}
        onPress={onPress}
      >
        <Text>{ placeholder || 'Continue' }</Text>
      </NBButton>
    );
  }
}

export default connectStyle('Portal.Button', styles, Button);
