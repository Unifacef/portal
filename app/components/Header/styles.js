import { Platform } from 'react-native';
import { em } from '../../utils';

export default {
  'NativeBase.ViewNB': {
    shadowOffset: {
      height: 4.5,
      width: 0,
    },
    shadowOpacity: 0.6,
    shadowColor: '#d6d6d6',
    elevation: Platform.OS === 'android' ? 4 : 3,
    paddingHorizontal: 5,
    'NativeBase.Header': {
      width: '100%',
      backgroundColor: '#FFFFFF',
      borderBottomWidth: 0,
      marginTop: 10,
      paddingRight: 0,
      paddingLeft: 0,
      'NativeBase.Left': {
        flex: 20,
      },
      'NativeBase.Right': {
        flex: 70,
      },
    },
    'NativeBase.ViewNB': {
      width: '100%',
      alignItems: 'flex-end',
      paddingRight: 6,
      paddingLeft: Platform.OS === 'android' ? 16 : 7,
      paddingBottom: 20,
      flexDirection: 'row',
      '.justTitle': {
        paddingVertical: 5,
        height: 115,
        justifyContent: 'space-between',
      },
      'NativeBase.Title': {
        fontFamily: 'Gibson',
        fontSize: Platform.OS === 'ios' ? em(2.5) : em(3),
        fontWeight: 'normal',
        color: '#454F63',
      },
    },  
  },
  '.noShadow': {
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 0,
    elevation: 0,
  },
  zIndex: 1,
  alignItems: 'center',
  justifyContent: 'center',
};
