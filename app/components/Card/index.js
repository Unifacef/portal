import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import { Card as NBCard } from 'native-base';

import styles from './styles';
import { connectStyle, computeProps } from '../../utils';

class Card extends PureComponent {
  render() {
    const { style } = computeProps(this.props);
    const { children, onPress } = this.props;

    if (onPress) {
      return (
        <TouchableOpacity onPress={() => onPress()} activeOpacity={0.7}>
          <NBCard style={style} {...this.props}>
            { children }
          </NBCard>
        </TouchableOpacity>
      );
    }

    return (
      <NBCard style={style} {...this.props}>
        { children }
      </NBCard>
    )
  }
}

export default connectStyle('Portal.Card', styles, Card);
