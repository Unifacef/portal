// @flow

import variable from "./../";

export default (variables /*: * */ = variable) => {
  const spinnerTheme = {
    height: 80
  };

  return spinnerTheme;
};
