import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import { Header as NBHeader, Left, Button, Body, Right, Title, View } from 'native-base';
import { Icon } from '..';

import { back } from '../../services';
import { em, connectStyle, computeProps } from '../../utils';
import styles from './styles';

class Header extends PureComponent {
  render() {
    const { style } = computeProps(this.props);
    const {
      title,
      onPressLeft,
      leftIcon,
      renderRight,
      renderHeader,
      noShadow,
      transparent,
      onPressRight,
      rightIcon,
    } = this.props;

    return (
      <View style={[style, { backgroundColor: transparent ? 'transparent' : '#fff' }]}>
        { renderHeader ?
          <NBHeader
            noShadow
            iosBarStyle="dark-content"
            androidStatusBarColor="#fff"
          >
            <Left>
              <Button onPress={() => { onPressLeft ? onPressLeft() : back() }} transparent>
                <Icon name={ leftIcon ? leftIcon : 'arrow-left' } />
              </Button>
            </Left>
            <Body />
            <Right>{ renderRight }</Right>
          </NBHeader>
          : null
        }
        <View justTitle={!renderHeader} noShadow={noShadow}>
          <Title>{ title }</Title>
          { onPressRight ?
            <TouchableOpacity onPress={() => onPressRight()}>
              <Icon
                style={{ marginBottom: 6, marginRight: 5 }}
                onPress={() => onPressRight()}
                name={ rightIcon || 'logout' }
                size={em(1.5)}
              />
            </TouchableOpacity>
            : null
          }
        </View>
      </View>
    )
  }
}

export default connectStyle('Portal.Header', styles, Header);
