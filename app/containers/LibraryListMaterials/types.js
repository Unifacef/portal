import { types, async } from '../../utils/typeCreator';

export default types([
  ...async('GET_AUTHORS'),
  ...async('GET_DATA'),
  ...async('GET_ASSUNTS'),
  ...async('GET_EXAMPLES'),
  'SET_MATERIAL',
], 'LIBRARY_MATERIALS');
