import React, { PureComponent } from 'react';
import { Platform, TouchableOpacity, Keyboard } from 'react-native';
import { Icon, Item, Input } from 'native-base';

import { computeProps, connectStyle, If, em } from '../../utils';
import { Card } from '../';
import styles from './styles';

class CardInput extends PureComponent {
  state = {
    showInput: true,
  };

  showInput = () => {
    Keyboard.dismiss();
    this.setState({ showInput: !this.state.showInput });
  }

  render() {
    const { style } = computeProps(this.props);
    const { value, onChangeText, name, placeholder, setClearStateField } = this.props;

    return (
      <Card onPress={this.showInput} style={style}>
        <Item input>
          <Input
            autoCapitalize="none"
            placeholder={placeholder}
            name={name}
            value={value}
            onChangeText={onChangeText(name)}
            placeholderTextColor="#454F63"
          />
          <If condition={value.length}>
            {() => (
              <TouchableOpacity onPress={() => setClearStateField(name)}>
                <Icon
                  name='close'
                  style={{
                    fontSize: em(2),
                    color: '#454F63',
                    paddingTop: Platform.OS =='ios' ? 5 : 0,
                  }}
                />
              </TouchableOpacity>
            )}
          </If>
        </Item>
      </Card>
    );
  }
}

export default connectStyle('Portal.CardInput', styles, CardInput);
