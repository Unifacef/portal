import { handleActions } from 'redux-actions';
import types from './types';

const INITIAL_STATE = {
  currentSemester: '',
  currentYear: '',
};

const reducer = handleActions(
  {
    [types.SET_CURRENT_YEAR]: (state, { currentSemester, currentYear }) => ({
      ...state,
      currentSemester: currentSemester,
      currentYear: currentYear,
    }),
  },
  INITIAL_STATE,
);

export default reducer;

