import React from 'react';
import { Platform } from 'react-native';
import { Picker } from '../';

const DataPicker = ({ data, label, selected, onChange }) => {
  const picker_data = data.map(({ id, name }) => ({
    label: name,
    value: name,
  }));

  return (
    <Picker
      selected={selected}
      onChange={onChange}
      options={Platform.select({
        ios: picker_data,
        android: [{ label: `${label}...`, value: null }, ...picker_data],
      })}
      placeholder={`${label}...`}
      title={label}
    />
  );
};

export default DataPicker;
