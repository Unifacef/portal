import { toSoapArgs, createEnvelope, headers } from '../../boot/config';
import { alert } from '../../utils';
import { navigate } from '../../services';
import types from './types';

export function getAllTypesMaterial() {
  return (dispatch) => {
    dispatch({
      type: types.GET_ALL_TYPES,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/getTodosMaterialTipo'),
          data: createEnvelope('getTodosMaterialTipo', toSoapArgs([])),
        },
      },
    })
    .catch((err) => {
      console.log(err);
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não conseguimos recuperar os examplares neste momento. Tente novamente, por favor.'
      });
    });
  }
}

export function setCurrentMaterialType(materialType) {
  return {
    type: types.SET_MATERIAL_TYPE,
    materialType,
  };
}

export function makeCompletlySearch(params) {
  return (dispatch, getSate) => {
    const title = params.title == '' ? '%' : `%${params.title}%`;
    const author = params.author == '' ? '%' : `%${params.author}%`;
    const assunt = params.assunt == '' ? '%' : `%${params.assunt}%`;
    const { materialType, types: typesMaterial } = getSate().librarySearch;
    const idType = typesMaterial.find(item => item.name === materialType);
    
    if (!idType) {
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! É necessário escolher um tipo de material para realizar a pesquisa.'
      });
    }

    if (assunt == '%' && author == '%' && title == '%') {
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! É necessário o preenchimento de pelo menos um campo para realizar a pesquisa.'
      });
    }

    dispatch({
      type: types.MAKE_SEARCH,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/localizarMaterialPorTituloAutorAssunto'),
          data: createEnvelope('localizarMaterialPorTituloAutorAssunto', toSoapArgs([title, author, assunt, idType, false])),
        },
      },
    })
    .then(() => navigate('LibraryListMaterials'))
    .catch((err) => {
      console.log(err);
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não foi possível realizar a sua busca neste momento. Tente novamente, por favor.'
      });
    });
  };
}

export function setValueToState(input, value) {
  return { type: types.SET_VALUE, input, value };
}

export function clearSearch() {
  return { type: types.CLEAR_STATE };
}
