import { em } from '../../utils';

export default {
  width: '100%',
  borderBottomWidth: 0.5,
  borderBottomColor: '#F2F2F2',
  borderTopWidth: 0,
  borderTopColor: '#F2F2F2',
  flexDirection: 'row',
  '.last': {
    borderBottomWidth: 0,
  },
  height: 50,
  paddingLeft: 15,
  paddingVerical: 10,
  'NativeBase.Left': {
    flex: 80,

    justifyContent: 'center',
    'NativeBase.Text': {
      color: '#454F63',
      fontFamily: 'Gibson',
      fontWeight: '400',
      fontSize: em(1.4),
    },
  },
  'NativeBase.Right': {
    flex: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
};
