import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { Content } from 'native-base';

import { TicketsItem, Card, Balls } from '../../../../components';

class TicketsList extends Component {
  _keyExtractor = (item) => item.id;

  getParcela = (item, tickets, index) => {
    if (item.parcela != undefined) {
      if (item.parcela == 0 && tickets[index + 1] != undefined) {
        return tickets[index + 1].parcela;
      }
      return item.parcela;
    }
  }

  render() {
    const { tickets } = this.props;

    return (
      <Content padder>
        <Balls
          titleGreen="Em aberto"
          titleBlue="Quitada"
          titleRed="Vencida"
          titleGray='Renegociada'
          titleLightGray='Cancelada'
        />
        <FlatList
          data={tickets}
          keyExtractor={this._keyExtractor}
          renderItem={({item, index}) => {
            const parcelas = tickets.filter((ticket, i) => item.documento.substr(3, 7) == ticket.documento.substr(3, 7));
            let status = 'opened';

            if (parcelas.length > 1 && item.valorPago == '0.0') {
              status = 'cancelada';
            } else if (item.parcela == 0 && item.valorPago != '0.0') {
              status = 'renegociada';
            } else if (item.valorPago !== '0.0') {
              status = 'quitada';
            }

            return (
              <Card key={index} list margedHorizontal>
                <TicketsItem
                  last={index === (tickets.length - 1)}
                  status={status}
                  dueDate={item.documento}
                  value={item.valor}
                  valueLiquid={item.valor1}
                  datePay={item.situacao}
                  valuePay={item.valorPago}
                  parcela={this.getParcela(item, tickets, index)}
                  numberTicket={typeof item.nome === 'object' ? false : item.nome}
                />
              </Card>
          )}}
        />
      </Content>
    );
  }
}

export default TicketsList;
