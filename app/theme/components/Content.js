// @flow

import variable from "./../";

export default (variables /*: * */ = variable) => {
  const contentTheme = {
    flex: 1,
    backgroundColor: 'transparent',
    paddingBottom: 0,
    "NativeBase.Segment": {
      borderWidth: 0,
      backgroundColor: "transparent"
    },
    '.padderVertical': {
      paddingVertical: 16,
    },
    '.margedHorizontal': {
      marginHorizontal: 16,
    },
    '.margedVertical': {
      marginVertical: 16,
    },
    '.padderTop': {
      paddingTop: 16,
    },
    '.margedBottom': {
      marginBottom: 16,
    },
  };

  return contentTheme;
};
