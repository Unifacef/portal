import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { Header, Spinner } from '../../components';
import CardsImages from './components/CardsImages';
import { navigate } from '../../services';
import { logout } from '../Login/actions';

class Home extends Component {
  render() {
    if (this.props.isLoadingLogin) return <Spinner />;

    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        style={{ flex: 1 }}
        colors={['#FFF', '#F7F7FA']}
      >
        <Header
          title="Portal Uni-Facef"
          onPressRight={this.props.logout}
          noShadow
          transparent
          style={{ height: 130 }}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <CardsImages
            navigate={navigate}
            user={this.props.user}
          />
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = ({ login }) => ({
  user: login.userActual,
  isLoadingLogin: login.isLoading,
});

const mapDispatchToProps = {
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
