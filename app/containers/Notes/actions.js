import { toSoapArgs, createEnvelope, headers } from '../../boot/config';
import { alert } from '../../utils';
import types from './types';

export function getNotes(semester) {
  return (dispatch) => {
    dispatch({
      type: types.GET_NOTES,
      payload: {
        request: {
          url: 'NotaWS/NotaWS',
          method: 'POST',
          headers: headers('NotaWS/getNotasPorMatricula'),
          data: createEnvelope('getNotasPorMatricula', toSoapArgs([semester])),
        },
      },
    })
    .catch(() => {
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não conseguimos recuperar suas notas neste momento. Tente novamente, por favor.'
      });
    });
  };
}
