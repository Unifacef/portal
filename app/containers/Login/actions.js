import { AsyncStorage } from 'react-native';
import types from './types';
import { reset } from '../../services';
import { toSoapArgs, createEnvelope, headers, getIsMatriculaValidaReturn, xmlToJSON } from '../../boot/config';
import { alert } from '../../utils';

export function stopLoading() {
  return { type: types.AUTH_STOP_LOADING };
}

export function setUser(user) {
  return { type: types.AUTH_SET_USER, user };
}

export function getUser() {
  return (dispatch, getState) => {
    const { code, user } = getState().login;

    dispatch({
      type: types.AUTH_GET_USER,
      payload: {
        request: {
          url: 'NotaWS/NotaWS',
          method: 'POST',
          headers: headers('NotaWS/getMatriculasPorPessoa'),
          data: createEnvelope('getMatriculasPorPessoa', toSoapArgs([code || user[user.length - 1].pessoa])),
        },
      },
    }).then(async ({ payload: { data } }) => {
      const result = xmlToJSON(data, 'getMatriculasPorPessoa');
      const currentUserSemester = Array.isArray(result) ? result[result.length - 1] : result;
      await AsyncStorage.setItem('@portal/token', JSON.stringify(currentUserSemester));
      await AsyncStorage.setItem('@portal/user', JSON.stringify(Array.isArray(result) ? result : [currentUserSemester]));
      dispatch(setUser(Array.isArray(result) ? result : [currentUserSemester]));
      dispatch(setToken(currentUserSemester));
    });
  };
}

export function makeLogin() {
  return (dispatch, getState) => {
    const { code, password } = getState().login;

    if (code === '' || password === '') {
      return alert({
        title: 'Portal Uni-Facef',
        text: 'É necessário que todos os campos estejam preenchidos.',
      });
    }

    return dispatch({
      type: types.AUTH_REQUEST,
      payload: {
        request: {
          url: 'NotaWS/NotaWS',
          method: 'POST',
          headers: headers('NotaWS/isMatriculaValida'),
          data: createEnvelope('isMatriculaValida', toSoapArgs([code, password])),
        },
      },
    })
    .then(({ payload: { data }}) => {
      switch(getIsMatriculaValidaReturn(data)) {
        case 0: {
          alert({
            title: 'Portal Uni-Facef',
            text: 'É necessário que todos os campos estejam preenchidos.',
          });
          return setTimeout(() => dispatch(stopLoading()), 1000);
        }
        case 1: {
          alert({
            title: 'Portal Uni-Facef',
            text: 'Opa! Parece que seus dados não batem com os da nossa base. Tente novamente, por favor.',
          });
          return setTimeout(() => dispatch(stopLoading()), 1000);
        }
        case 2: {
          dispatch(getUser());
          return reset('Home');
        }
      }
    })
    .catch(error => {
      console.error(error);
      dispatch(
        alert({
          title: 'Portal Uni-Facef',
          text: 'Opa! parece que você esta sem conexão com a internet.',
        }),
      );
    });
  };
}

export function setToken(token) {
  return { type: types.AUTH_SET_TOKEN, token };
}

export function verifyLogin() {
  return dispatch =>
    AsyncStorage.getItem('@portal/token')
      .then(async (token) => {
        if (!token) {
          throw new Error('Token not found!');
        }
        await AsyncStorage.getItem('@portal/user').then((res) => dispatch(setUser(JSON.parse(res))));
        dispatch(setToken(JSON.parse(token)));
        dispatch(getUser());
      })
      .then(() => reset('Home'))
      .catch(error => {
        console.error(error);
        dispatch(stopLoading());
      });
}

export function setValueToState(input, value) {
  return {
    type: types.SET_VALUE,
    input,
    value,
  };
}

export function logout() {
  return dispatch => {
    alert({
      title: 'Portal Uni-Facef',
      text: 'Deseja realmente sair?',
      yesOrNo: true, 
      yes: () => AsyncStorage.clear().then(() => { reset('Login'); dispatch({ type: types.LOGOUT })}),
    });
  };
}