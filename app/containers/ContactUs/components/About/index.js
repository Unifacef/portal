import React, { Component } from 'react';
import { View, Left, Text, Content } from 'native-base';

import { em } from '../../../../utils';
import { Card, Container, CollapsibleChild } from '../../../../components';

class About extends Component {
  renderCardContact = (title, text) => (
    <Card margedHorizontal padderAll>
      <Text textTitle>{title}</Text>
      <View author margedTop>
        <Left><Text textInfo>{text}</Text></Left>
      </View>
    </Card>
  );

  render() {
    return (
      <Container gray>
        <Content
          showsVerticalScrollIndicator={false}
          margedBottom
          padderTop
        >
          <CollapsibleChild
            title='Ver Contatos'
            content={() => (
              <>
                {this.renderCardContact('Administração', 'Ms. Francismar Monteiro\nfrancismar@facef.br')}
                {this.renderCardContact('Ciências Contábeis', 'Ms. Orivaldo Donzelli\norivaldo@facef.br')}
                {this.renderCardContact('Ciências Econômicas', 'Ms. Ana Tereza Jacinto Teixeira\nanatereza@facef.br')}
                {this.renderCardContact('Comunicação Social', 'Ms. Paulo Anderson Cinti\npaulocinti@facef.br')}
                {this.renderCardContact('Engenharia Civil', 'Dr. João Baptista Comparini\njcomparini@facef.br')}
                {this.renderCardContact('Engenharia de Produção', 'Dr. Antônio Carlos Tambellini Bettarello\nacbettarello@facef.br')}
                {this.renderCardContact('Letras', 'Ms. Ana Lúcia Furquim Campos Toscano\nanalucia@facef.br')}
                {this.renderCardContact('Matemática', 'Ms. Silvia Regina Viel\nsilviaviel@facef.br')}
                {this.renderCardContact('Psicologia', 'Ms. Maria Cherubina de Lima Alves\nmcherubina@facef.br')}
                {this.renderCardContact('Sistemas de Informação', 'Dr. Daniel Facciolo Pires\ndaniel@facef.br')}
              </>
            )}
          />
          <CollapsibleChild
            title='Ver Departamentos'
            content={() => (
              <>
                {this.renderCardContact('Reitoria', 'reitoria@facef.br – Ramal 630')}
                {this.renderCardContact('Pró-Reitoria de Administração', 'pradministracao@facef.br – Ramal 632')}
                {this.renderCardContact('Pró-Reitoria Acadêmica', 'sheila@facef.br – Ramal 628')}
                {this.renderCardContact('Pós-Graduação', 'posgraduacao@facef.br – Ramal 618')}
                {this.renderCardContact('Biblioteca', 'biblioteca@facef.br – Ramal 610')}
                {this.renderCardContact('Compras', 'compras@facef.br – Ramal 603')}
                {this.renderCardContact('Informática', 'informatica@facef.br – Ramal 621/641')}
                {this.renderCardContact('Instituto', 'pesquisa@facef.br – Ramal 609')}
                {this.renderCardContact('Jurídico', 'guedine@facef.br – Ramal 617')}
                {this.renderCardContact('Clínica de Psicologia', 'clinica@facef.br – Ramal 611')}
                {this.renderCardContact('Recursos Humanos', 'recursoshumanos@facef.br – Ramal 602')}
                {this.renderCardContact('Secretaria', 'secretaria@facef.br – Ramal 604/624/647')}
                {this.renderCardContact('Tesouraria', 'tesouraria@facef.br – Ramal 606')}
                {this.renderCardContact('Ouvidoria', 'ouvidoria@facef.br')}
              </>
            )}
          />
          <View>
            <Card margedHorizontal padderAll>
              <Text textTitle style={{ fontSize: em(1.25) }}>Dúvidas, Sugestões, Reclamações e FAQ</Text>
              <View author margedTop>
                <Left>
                  <Text textInfo>Pedro Galetti</Text>
                </Left>
              </View>
              <View author>
                <Left>
                  <Text textInfo>E-mail: pedro.unifacef@gmail.com</Text>
                </Left>
              </View>
              <View author>
                <Left>
                  <Text textInfo>Telefone: (16) 98190-8155</Text>
                </Left>
              </View>
            </Card>
          </View>
          <View margedBottom>
            <Card margedHorizontal padderAll>
              <Text textTitle style={{ fontSize: em(1.25) }}>Centro Universitário Municipal de Franca</Text>
              <View author margedTop>
                <Left>
                  <Text textInfo>CNPJ: 47.987.136/0001-09</Text>
                </Left>
              </View>
              <View author>
                <Left>
                  <Text textInfo>Inscrição Estadual: Isento</Text>
                </Left>
              </View>
              <View author>
                <Left>
                  <Text textInfo>Telefone: 0800 940 4688 / (16) 3713-4688</Text>
                </Left>
              </View>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

export default About;
