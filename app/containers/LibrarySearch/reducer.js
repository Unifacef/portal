import { handleActions, combineActions } from 'redux-actions';
import { xmlToJSON } from '../../boot/config';
import types from './types';

const INITIAL_STATE = {
  isLoading: true,
  types: [],
  materialType: 'MATERIAL',
  examples: [],
  search: {
    title: '',
    author: '',
    assunt: '',
  },
};

const beginLoading = combineActions(
  types.GET_ALL_TYPES,
  types.MAKE_SEARCH,
);

const stopLoading = combineActions(
  types.GET_ALL_TYPES_SUCCESS,
  types.GET_ALL_TYPES_FAIL,
  types.MAKE_SEARCH_SUCCESS,
  types.MAKE_SEARCH_FAIL,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),    
    [types.SET_VALUE]: (state,  { input, value }) => ({
      ...state,
      search: {
        ...state.search,
        [input]: value,
      },
    }),
    [types.SET_MATERIAL_TYPE]: (state, { materialType }) => ({
      ...state,
      materialType,
    }),
    [types.GET_ALL_TYPES_SUCCESS]: (state, { payload: { data } }) => {
      let types = [];
      xmlToJSON(data, 'getTodosMaterialTipo').map((item) => {
        types.push({ name: item.nome, id: item.id, });
      });

      return {
        ...state,
        types,
      };
    },
    [types.MAKE_SEARCH_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      examples: xmlToJSON(data, 'localizarMaterialPorTituloAutorAssunto'),
    }),
    [types.CLEAR_STATE]: () => ({ ...INITIAL_STATE }),
  },
  INITIAL_STATE,
);

export default reducer;
