import React, { PureComponent } from 'react'
import { FlatList } from 'react-native';

import { List } from '../../../../components';
import { toCapitalize, selectIcon, connectStyle } from '../../../../utils';

class Materials extends PureComponent {
  render() {
    const { data, onPress, materialType } = this.props;

    return (
      <FlatList
        data={data}
        renderItem={({ item }) => {
          return (
            <List
              leftIcon={selectIcon(materialType)}
              key={item.id}
              onPress={() => onPress(item)}
              text={toCapitalize(item.material)}
            />
          );
        }}
      />
    );
  }
}

export default connectStyle('Portal.Materials', {}, Materials);
