import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';

import { toCapitalize, connectStyle} from '../../utils';
import { RadioButtonList } from '../';

class ListButton extends PureComponent {
  _keyExtractor = (item) => item.id;

  render() {
    const { onPress, itemSelected, data } = this.props;

    return (
      <FlatList
        data={data}
        keyExtractor={this._keyExtractor}
        renderItem={({item, index}) => {
          return (
            <RadioButtonList
              key={index}
              last={index === (data.length - 1)}
              onPress={() => onPress(item.name)}
              checked={itemSelected === item.name}
              text={toCapitalize(item.name)}
            />
          );
        }}
      />
    );
  }
}

export default connectStyle('Portal.ListButton', {}, ListButton);
