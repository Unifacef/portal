// @flow

import variable from "./../";

export default (variables /*: * */ = variable) => {
  const tabTheme = {
    flex: 1,
    backgroundColor: "#FFF"
  };

  return tabTheme;
};
