import { toSoapArgs, createEnvelope, headers } from '../../boot/config';
import { alert } from '../../utils';
import types from './types';

export function getTickets(semester) {
  return (dispatch) => {
    dispatch({
      type: types.GET_TICKETS,
      payload: {
        request: {
          url: 'RecebedoriaWS/RecebedoriaWS',
          method: 'POST',
          headers: headers('RecebedoriaWS/getMensalidades'),
          data: createEnvelope('getMensalidades', toSoapArgs([semester])),
        },
      },
    })
    .catch((err) => {
      console.log(err);
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não conseguimos recuperar seus boletos neste momento. Tente novamente, por favor.'
      });
    });
  };
}