export default {
  '.searchButton': {
    width: '100%',
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: '#454F63'
  },
  '.block': {
    justifyContent: "center",
    alignSelf: "stretch",
    backgroundColor: '#454F63'
  },
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.16,
  shadowRadius: 3.84,
  elevation: 2,
  borderRadius: 15,
  marginVertical: 30,
  alignItems: 'center',
  justifyContent: 'center',
};
