import React, { Component } from 'react'
import { View } from 'native-base';

import { computeProps, connectStyle } from '../../utils';

class Ball extends Component {
  render() {
    const { style } = computeProps(this.props);
    const { color, size } = this.props;

    return (
      <View
        style={[style, {
          backgroundColor: color || '#3497FD',
          height: size || 10,
          width: size || 10,
          borderRadius: size || 10,
          marginHorizontal: 5,
        }]}
      />
    )
  }
}

export default connectStyle('Portal.Ball', {}, Ball);