import React, { Component } from 'react'
import { View, Text } from 'native-base';

import styles from './styles';
import { computeProps, connectStyle } from '../../utils';
import { Ball } from '../';

class Balls extends Component {
  render() {
    const { style } = computeProps(this.props);
    const { titleBlue, titleRed, titleGreen, titleGray, titleLightGray } = this.props;

    return (
      <View style={style}>
        <View>
          <View>
            <Ball color="#3ACCE1" />
            <Text>{titleGreen}</Text>
          </View>
          <View>
            <Ball />
            <Text>{titleBlue}</Text>
          </View>
          <View>
            <Ball color="rgba(255, 0, 0, 0.84)" />
            <Text>{titleRed}</Text>
          </View>
        </View>
        <View>
          {titleLightGray
            ? (<View>
              <Ball color="#d3d3d3" />
              <Text>{titleLightGray}</Text>
            </View>)
            : null
          }
          {titleGray
            ? (<View>
              <Ball color="gray" />
              <Text>{titleGray}</Text>
            </View>)
            : null
          }
          {titleLightGray && titleGray ? <View>
            <Ball color="transparent" />
            <Text></Text>
          </View>
            : null}
        </View>
      </View>
    )
  }
}

export default connectStyle('Portal.Balls', styles, Balls);