import React, { Component } from 'react'
import { Text } from 'native-base';

import styles from './styles';
import { connectStyle, computeProps } from '../../utils';
import { Image, Card } from '..';

class CardImage extends Component {
  render() {
    const { style, styleSubTitle } = computeProps(this.props);
    const { source, onPress, title, subTitle } = this.props;

    return (
      <Card onPress={onPress}>
        <Image source={source} style={style} />
        <Text textCard titleCard>{ title || '' }</Text>
        {subTitle ? <Text textCard subTitleCard style={styleSubTitle}>{ subTitle || String(new Date()) }</Text> : null}
      </Card>
    );
  }
}

export default connectStyle('Portal.CardImage', styles, CardImage);
