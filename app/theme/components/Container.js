// @flow

import { Platform, Dimensions } from "react-native";

import variable from "./../";

const deviceHeight = Dimensions.get("window").height;
export default (variables /*: * */ = variable) => {
  const theme = {
    flex: 1,
    height: Platform.OS === "ios" ? deviceHeight : deviceHeight - 20,
    backgroundColor: '#FFF',
    '.gray': {
      backgroundColor: '#F7F7FA',
    },
    '.margedBottom': {
      marginBottom: 16,
    },
  };

  return theme;
};
