import React, { Component } from 'react';
import { View, TouchableOpacity, Platform } from 'react-native';
import { Left, Right, Text } from 'native-base';

import { RadioButton } from '../';
import { connectStyle, computeProps } from '../../utils';
import styles from './styles';

class SelectItem extends Component {
  render() {
    const { style } = computeProps(this.props);
    const { onPress, checked, text, last } = this.props;
    
    return (
      <TouchableOpacity onPress={onPress}>
        <View last={last} style={style}>
          <Left>
            <Text>{text.length > 22 && Platform.OS === 'ios' ? `${text.substring(0, 20)}...` : text}</Text>
          </Left>
          <Right>
            <RadioButton
              onPress={onPress}
              checked={checked}
            />
          </Right>
        </View>
      </TouchableOpacity>
    );
  }
}

export default connectStyle('Portal.SelectItem', styles, SelectItem);
