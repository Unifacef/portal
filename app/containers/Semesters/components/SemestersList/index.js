import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import { Content } from 'native-base';

import { toCapitalize } from '../../../../utils';
import { List } from '../../../../components';

class SemestersList extends PureComponent {
  _keyExtractor = (item) => item.id;

  render() {
    const { semesters, onPressSemester } = this.props;

    return (
      <Content padder>
        <FlatList
          data={semesters}
          keyExtractor={this._keyExtractor}
          renderItem={({item}) => {
            return (
              <List
                key={item.id}
                onPress={() => onPressSemester(item.id)}
                text={toCapitalize(item.serie)}
              />
            );
          }}
        />
      </Content>
    );
  }
}

export default SemestersList;
