import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { Content } from 'native-base';

import { Balls, Card, NotesItem } from '../../../../components';
import { toCapitalize } from '../../../../utils';

class NotesList extends Component {
  _keyExtractor = (item, index) => index.toString();

  render() {
    const { notes, isFromMedicine } = this.props;

    return (
      <Content padder>
        <Balls
          titleGreen="Cursando"
          titleBlue="Aprovado"
          titleRed="Reprovado"
        />
        <FlatList
          data={notes}
          keyExtractor={this._keyExtractor}
          renderItem={({item, index}) => {
            if (item.ativo && item.situacao != 'APROVEITAMENTO') {
              return (
                <Card key={index} list margedHorizontal>
                  <NotesItem
                    isFromMedicine={isFromMedicine}
                    last={index === (notes.length - 1)}
                    title={toCapitalize(item.nomeDisciplina)}
                    approved={item.situacao === 'APROVADO'}
                    cursed={item.situacao === 'CURSANDO' || item.situacao === '-' || item.situacao == 'DEPENDENCIA'}
                    noteOne={item.nota1}
                    noteTwo={item.nota2}
                    noteThree={item.nota3}
                    noteFour={item.nota4}
                    sub={item.recupera1}
                    faultsOne={item.falta1}
                    faultsTwo={item.falta2}
                    frequency={item.frequencia}
                    average={item.media}
                  />
                </Card>
              );
            }
          }}
        />
      </Content>
    );
  }
}

export default NotesList;
