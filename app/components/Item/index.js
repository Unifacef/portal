import React, { Component } from 'react'
import { Item as NBItem } from 'native-base';

import { computeProps, connectStyle } from '../../utils';
import styles from './styles';

class Item extends Component {
  render() {
    const { style } = computeProps(this.props);
    const { noBorder, children } = this.props;

    return (
      <NBItem style={style} noBorder={noBorder}>
        {children}
      </NBItem>
    )
  }
}

export default connectStyle('Portal.Item', styles, Item);