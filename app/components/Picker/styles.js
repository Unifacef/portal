import { Dimensions, Platform } from 'react-native';
import { em } from '../../utils';

export default {
  'NativeBase.Picker': {
    flex: 1,
    height: 40,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 0,
    borderRadius: em(3.8),
    borderColor: '#454F63',
    width: Dimensions.get('window').width - 30,
    ...Platform.select({
      android: {
        color: '#454F63',
      },
    }),
  },
};
