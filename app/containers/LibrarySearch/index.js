import React, { Component } from 'react';
import { connect } from 'react-redux';

import { ContainerList, Spinner } from '../../components';
import Search from './components/Search';
import * as actions from './actions';

class LibrarySearch extends Component {
  constructor(props) {
    super(props);
    this.props.getAllTypesMaterial();
    this.props.clearSearch();
  }

  setSearchState = field => value => this.props.setValueToState(field, value);

  setClearStateField = name => this.props.setValueToState(name, '');

  onPressSearch = () => {
    this.props.makeCompletlySearch({
      title: this.props.title,
      author: this.props.author,
      assunt: this.props.assunt,
    });
  }

  onSelectedMaterialType = (materialType) =>
    this.props.setCurrentMaterialType(materialType);

  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    }

    return (
      <ContainerList
        renderHeader
        title="Biblioteca"
      >
        <Search
          title={this.props.title}
          author={this.props.author}
          assunt={this.props.assunt}
          types={this.props.types}
          materialType={this.props.materialType}
          onPressSearch={this.onPressSearch}
          onSelectedMaterialType={this.onSelectedMaterialType}
          onChangeInput={this.setSearchState}
          setClearStateField={this.setClearStateField}
          isLoading={this.props.isLoading}
        />
      </ContainerList>
    );
  }
}

const mapStateToProps = ({ librarySearch }) => ({
  isLoading: librarySearch.isLoading,
  title: librarySearch.search.title,
  author: librarySearch.search.author,
  assunt: librarySearch.search.assunt,
  types: librarySearch.types,
  materialType: librarySearch.materialType,
});

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(LibrarySearch);
