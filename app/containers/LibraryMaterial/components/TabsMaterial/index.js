import React, { Component } from 'react';
import { Tabs, Tab } from 'native-base';

import styles from './styles';
import { Examples, Assunts, General } from '../';

class TabsMaterial extends Component {
  render() {
    const {
      material,
      currentData,
      currentAuthors,
      currentAssunts,
      currentExamples,
    } = this.props;

    return (
      <Tabs
        initialPage={0}
        tabBarUnderlineStyle={styles.tabs}
      >
        <Tab
          textStyle={[styles.textTab, {
            opacity: 0.4,
          }]}
          activeTextStyle={[styles.textTab, {
            opacity: 1,
          }]}
          tabStyle={styles.tab}
          activeTabStyle={styles.tab}
          heading="GERAL"
        >
          <General
            material={material}
            data={currentData}
            authors={currentAuthors}
          />
        </Tab>
        <Tab
          textStyle={[styles.textTab, {
            opacity: 0.4,
          }]}
          activeTextStyle={[styles.textTab, {
            opacity: 1,
          }]}
          tabStyle={styles.tab}
          activeTabStyle={styles.tab}
          heading="ASSUNTOS"
        >
          <Assunts assunts={currentAssunts} />
        </Tab>
        <Tab
          textStyle={[styles.textTab, {
            opacity: 0.4,
          }]}
          activeTextStyle={[styles.textTab, {
            opacity: 1,
          }]}
          tabStyle={styles.tab}
          activeTabStyle={styles.tab}
          heading="EXEMPLARES"
        >
          <Examples examples={typeof currentExamples == 'object' ? [currentExamples] : currentExamples} />
        </Tab>
      </Tabs>
    )
  }
}

export default TabsMaterial;
