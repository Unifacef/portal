import { cond, equals, always, T } from 'ramda';

const selectIcon = cond([
  [equals('CD - COMPACT DISC'), always('disc')],
  [equals('CHAVE'), always('key-variant')],
  [equals('LIVRO'), always('book-open')],
  [equals('DISQUETE'), always('floppy')],
  [equals('DVD'), always('disc')],
  [equals('VIDEO'), always('video')],
  [equals('VIDEO TEXTO'), always('message-video')],
  [equals('MONOGRAFIA'), always('file-document-box')],
  [equals('FITA CASSETE'), always('film')],
  [T, always('file')],
]);

export default selectIcon;
