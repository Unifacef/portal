import { StackActions, NavigationActions } from 'react-navigation';

let instance;

function setContainer(container) {
  if (container) {
    instance = container;
  }
}

function dispatch(...args) {
  if (!instance) {
    throw new Error('[NavigatorService] Trying to dispatch task to container when there is no container...');
  }

  return instance.dispatch(...args);
}

function getRouteNameFromState(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getRouteNameFromState(route);
  }
  return route.routeName;
}


function reset(routeName, params = {}) {
  dispatch(StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        type: 'Navigation/NAVIGATE',
        routeName,
        params,
      }),
    ],
  }));
}

function navigate(routeName, params) {
  dispatch(NavigationActions.navigate({
    type: 'Navigation/NAVIGATE',
    routeName,
    params,
  }));
}

function back(key = null) {
  dispatch(NavigationActions.back({ key }));
}

function getCurrentRoute() {
  if (!instance || !instance.state.nav) {
    return null;
  }

  return instance.state.nav.routes[instance.state.nav.index] || null;
}

export default {
  setContainer,
  navigate,
  reset,
  getCurrentRoute,
  getRouteNameFromState,
  dispatch,
  back,
};

export {
  setContainer,
  navigate,
  reset,
  getCurrentRoute,
  getRouteNameFromState,
  dispatch,
  back,
};
