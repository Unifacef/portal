import React, { Component } from 'react'
import { View } from 'native-base';

import { CardImage } from '../../../../components';
import { ajustSemester } from '../../../../utils';
import moment from 'moment';

class CardsImages extends Component {
  render() {
    const { navigate, user } = this.props;

    return (
      <View padderAll>
        <CardImage
          onPress={() => navigate('LibrarySearch')}
          source={require('../../../../assets/images/library_reserv.jpg')}
          title="Biblioteca"
          subTitle="Buscar..."
        />
        <CardImage
          onPress={() => navigate('Semesters')}
          source={require('../../../../assets/images/library.jpeg')}
          title="Notas/Faltas"
          subTitle={user.serie ? ajustSemester(user.serie.slice(6)) : ''}
          styleSubTitle={{ left: 7 }}
        />
        <CardImage
          onPress={() => navigate('Finance')}
          source={require('../../../../assets/images/finance.png')}
          title="Financeiro"
          subTitle={moment().format('YYYY')}
        />
        <CardImage
          onPress={() => navigate('ContactUs')}
          source={require('../../../../assets/images/contact.jpg')}
          title="Fale Conosco"
          subTitle='Informações'
        />
      </View>
    );
  }
}

export default CardsImages;
