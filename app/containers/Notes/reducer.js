import { handleActions, combineActions } from 'redux-actions';
import { xmlToJSON } from '../../boot/config';
import types from './types';

const INITIAL_STATE = {
  isLoading: true,
  notes: [],
};

const beginLoading = combineActions(types.GET_NOTES);

const stopLoading = combineActions(
  types.GET_NOTES_SUCCESS,
  types.GET_NOTES_FAIL,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),
    [types.GET_NOTES_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      notes: xmlToJSON(data, 'getNotasPorMatricula'),
    }),
    [types.GET_NOTES_FAIL]: (state, { payload: { data } }) => ({
      ...state,
      notes: [],
    }),
  },
  INITIAL_STATE,
);

export default reducer;
