import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Root, Container, StyleProvider } from 'native-base';
import Orientation from 'react-native-orientation';
import { MenuProvider } from 'react-native-popup-menu';

import getTheme from './theme/components';
import Portal from './theme';

import { store } from './boot/store';
import Router from './Router';

console.disableYellowBox = true;

class Master extends Component {
  componentDidMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <StyleProvider style={getTheme(Portal)}>
        <Provider store={store}>
          <MenuProvider
            customStyles={{
              backdrop: {
                backgroundColor: '#000',
                opacity: 0.5,
              },
            }}
          >
            <Root>
              <Container>
                <Router />
              </Container>
            </Root>
          </MenuProvider>
        </Provider>
      </StyleProvider>
    );
  }
}

export default Master;
