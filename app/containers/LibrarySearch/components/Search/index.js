import React, { Component } from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';
import { Content } from 'native-base';

import { computeProps, connectStyle } from '../../../../utils';
import { Collapsible, Button, CardInput } from '../../../../components';
import styles from './styles';

class Search extends Component {
  onPressSearch = () => {
    Keyboard.dismiss();
    this.props.onPressSearch();
  }

  render() {
    const { style } = computeProps(this.props);
    const {
      title,
      author,
      assunt,
      types,
      materialType,
      onChangeInput,
      onSelectedMaterialType,
      setClearStateField,
    } = this.props;
    
    return (
      <Content
        contentContainerStyle={style}
        padder
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <>
            <Collapsible
              data={types}
              value={materialType}
              onPress={onSelectedMaterialType}
            />
            <CardInput
              placeholder="Título"
              name="title"
              value={title}
              onChangeText={onChangeInput}
              setClearStateField={setClearStateField}
            />
            <CardInput
              placeholder="Autor"
              name="author"
              value={author}
              onChangeText={onChangeInput}
              setClearStateField={setClearStateField}
            />
            <CardInput
              placeholder="Assunto"
              name="assunt"
              value={assunt}
              onChangeText={onChangeInput}
              setClearStateField={setClearStateField}
            />
            <Button
              searchButton
              placeholder="Buscar"
              onPress={this.onPressSearch}
            />
          </>
        </TouchableWithoutFeedback>
      </Content>
    );
  }
}

export default connectStyle('Portal.SearchLibrary', styles, Search);