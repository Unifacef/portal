import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Spinner, ContainerList } from '../../components';
import NotesList from './components/NotesList';
import { toCapitalize } from '../../utils';
import * as actions from './actions';

class Notes extends Component {
  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    }
    
    return (
      <ContainerList
        renderHeader
        title={toCapitalize(this.props.user.find(item => item.id === this.props.currentSemester).serie)}
      >
        <NotesList
          notes={this.props.notes}
          isFromMedicine={this.props.userActual.curso == 'MEDICINA'}
        />
      </ContainerList>
    );
  }
}

const mapStateToProps = ({ semesters, notes, login }) => ({
  currentSemester: semesters.currentSemester,
  isLoading: notes.isLoading,
  notes: notes.notes,
  user: login.user,
  userActual: login.userActual,
});

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Notes);
