import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';

import { Spinner } from '../../components';
import Form from './components/Form';
import * as actions from './actions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.props.verifyLogin();
  }

  setFormState = field => value => this.props.setValueToState(field, value);

  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    }

    return (
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['#FFF', '#F8F8FA']}
        style={{ flex: 1 }}
      >
        <Form
          isLoading={this.props.isLoading}
          code={this.props.code}
          password={this.props.password}
          onChangeInput={this.setFormState}
          onPressSignIn={() => this.props.makeLogin()}
        />
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => ({
  code: state.login.code,
  password: state.login.password,
  user: state.login.user,
  isLoading: state.login.isLoading,
});

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
