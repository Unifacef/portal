import { Platform } from 'react-native';
import { em } from '../../utils';

export default {
  'NativeBase.Item': {
    borderWidth: 0,
    marginBottom: 10,
    marginVertical: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.84,
    elevation: 2,
    borderRadius: 16,
    backgroundColor: '#fff',
    'NativeBase.Icon': {
      color: '#454F63',
      fontSize: em(1.27),
      paddingRight: 0,
      paddingHorizontal: 13,
      paddingTop: 5,
    },
    'NativeBase.Input': {
      fontSize: em(1),
      lineHeight: Platform.select({
        ios: 16,
        android: undefined,
      }),
      paddingLeft: 22,
    },
  },
};
