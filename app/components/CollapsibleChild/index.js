import React, { PureComponent } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import { View, Text } from 'native-base';
import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { em } from '../../utils';

class CollapsibleChild extends PureComponent {
  state = {
    isCollapsed: true,
  };

  handleCollapsible = () => this.setState({ isCollapsed: !this.state.isCollapsed });

  render() {
    return (
      <>
        <TouchableOpacity
          onPress={() => this.handleCollapsible()}
          style={{ flexDirection: 'row', flex: 1, alignItems: 'center', marginLeft: 16 }}
        >
          <Icon
            name={this.state.isCollapsed ? 'chevron-down' : 'chevron-up'}
            style={{ alignSelf: 'center', fontSize: em(2), color: '#78849E', paddingTop: Platform.OS == 'ios' ? 5 : 0 }}
          />
          <Text textInfo style={{ fontSize: em(1.4) }}>{this.props.title}</Text>
        </TouchableOpacity>
        <View margedBottom>
          <Collapsible collapsed={this.state.isCollapsed}>
            {this.props.content()}
          </Collapsible>
        </View>
      </>
    );
  }
}

export default CollapsibleChild;
