import { combineReducers } from 'redux';
import login from '../containers/Login/reducer';
import semesters from '../containers/Semesters/reducer';
import finance from '../containers/Finance/reducer';
import tickets from '../containers/Tickets/reducer';
import librarySearch from '../containers/LibrarySearch/reducer';
import libraryMaterials from '../containers/LibraryListMaterials/reducer';
import notes from '../containers/Notes/reducer';

export default combineReducers({
  login,
  semesters,
  finance,
  notes,
  tickets,
  librarySearch,
  libraryMaterials,
});
