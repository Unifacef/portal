import { types } from '../../utils/typeCreator';

export default types([
  'SET_CURRENT_YEAR',
], 'FINANCE');
