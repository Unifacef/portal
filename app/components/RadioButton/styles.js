export default {
  'NativeBase.ViewNB': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    'NativeBase.ViewNB': {
      height: 22,
      width: 22,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 2,
      borderColor: '#959595',
      'NativeBase.ViewNB': {
        width: 11,
        height: 11,
        borderRadius: 11,
        backgroundColor: '#454F63',
      },
    },
  },
}