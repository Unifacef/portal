import React, { Component } from 'react';
import { Image, Keyboard } from 'react-native';
import { Content } from 'native-base';

import styles from './styles';
import { connectStyle, computeProps } from '../../../../utils';
import { Input, Button } from '../../../../components';

class Form extends Component {
  state = {
    showPassword: true,
    iconPassword: 'eye',
  }

  showPassword() {
    this.setState({
      showPassword: !this.state.showPassword,
      iconPassword: this.state.iconPassword === 'eye-off' ? 'eye' : 'eye-off',
    });
  }

  onPressSignIn = () => {
    Keyboard.dismiss();
    return this.props.onPressSignIn();
  }

  render() {
    const { style } = computeProps(this.props);
    const {
      code,
      password,
      onChangeInput,
    } = this.props;

    return (
      <Content padder contentContainerStyle={style}>
        <Image
          style={styles.image}
          source={require('../../../../assets/images/logo.png')}
        />
        <Input
          autoCapitalize="none"
          placeholder="Código"
          name="code"
          value={code}
          onChangeText={onChangeInput('code')}
        />
        <Input
          secureTextEntry={this.state.showPassword}
          placeholder="Senha"
          name="password"
          value={password}
          onChangeText={onChangeInput('password')}
          iconRight={this.state.iconPassword}
          onPressIcon={() => this.showPassword()}
        />
        <Button
          block
          onPress={this.onPressSignIn}
          placeholder='ENTRAR'
        />
      </Content>
    );
  }
}

export default connectStyle('Portal.Form', styles, Form);
