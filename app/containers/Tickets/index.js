import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Spinner, ContainerList } from '../../components';
import TicketsList from './components/TickestList';

class Tickets extends Component {
  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    }
    
    return (
      <ContainerList
        renderHeader
        title={this.props.currentYear}
      >
        <TicketsList
          tickets={this.props.tickets}
        />
      </ContainerList>
    );
  }
}

const mapStateToProps = ({ tickets, finance }) => ({
  isLoading: tickets.isLoading,
  tickets: tickets.tickets,
  currentYear: finance.currentYear,
});

export default connect(mapStateToProps, {})(Tickets);
