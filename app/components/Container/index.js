import React, { Component } from 'react'
import { ScrollView } from 'react-native';
import { Container as NBContainer } from 'native-base';

class Container extends Component {
  render() {
    const { children } = this.props;

    return (
      <NBContainer {...this.props}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flex: 1 }}
          keyboardShouldPersistTaps="always"
        >
          { children }
        </ScrollView>
      </NBContainer>
    );
  }
}

export default Container;
