import React, { Component } from 'react'
import { Grid, Col, Row, View, Text, Left } from 'native-base';

import styles from './styles';
import { computeProps, connectStyle } from '../../utils';
import { Ball } from '../';

class TicketsItem extends Component {
  getStatus = () => {
    const { status } = this.props;

    if (status == 'renegociada') {
      return 'gray';
    }
    if (status == 'cancelada') {
      return '#d3d3d3';
    }
    if (status == 'quitada') {
      return '#3497FD';
    }
    if (status == 'opened') {
      return '#3ACCE1';
    }
    return 'rgba(255, 0, 0, 0.84';
  }

  render() {
    const { style } = computeProps(this.props);
    const {
      dueDate,
      value,
      valueLiquid,
      datePay,
      valuePay,
      numberTicket,
      last,
      parcela,
    } = this.props;

    return (
      <View style={style} last={last}>
        <View author margedBottom>
          <Left>
            <Ball color={this.getStatus()} />
            <Text textInfo>{numberTicket || 'Não gerado.'}</Text>
          </Left>
        </View>

        <Grid>
          <Row style={styles.row}>
            <Col>
              <Text textHeader>Valor</Text>
            </Col>
            <Col>
              <Text textHeader>Líquido</Text>
            </Col>
            <Col>
              <Text textHeader>Debitado</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textChild>R$ {Number(value).toFixed(2) || '0.0'}</Text>
            </Col>
            <Col>
              <Text textChild>R$ {Number(valueLiquid).toFixed(2) || '0.0'}</Text>
            </Col>
            <Col>
              <Text textChild>R$ {Number(valuePay).toFixed(2) || '0.0'}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textHeader>Pagamento</Text>
            </Col>
            <Col>
              <Text textHeader>Parcela</Text>
            </Col>
            <Col>
              <Text textHeader>Vencimento</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textChild>{datePay || 'Em aberto.'}</Text>
            </Col>
            <Col>
              <Text textChild>{ parcela || '' }</Text>
            </Col>
            <Col>
              <Text textChild>{dueDate}</Text>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default connectStyle('Portal.TicketsItem', styles, TicketsItem);
