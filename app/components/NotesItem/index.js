import React, { Component } from 'react'
import { View, Text, Left, Grid, Col, Row } from 'native-base';

import { Ball } from '../';
import styles from './styles';
import { connectStyle, computeProps } from '../../utils';

class NotesItem extends Component {
  render() {
    const { style } = computeProps(this.props);
    const {
      isFromMedicine,
      title,
      approved,
      noteOne,
      noteTwo,
      noteThree,
      noteFour,
      sub,
      average,
      faultsOne,
      faultsTwo,
      frequency,
      cursed,
    } = this.props;

    const gridComum = () => (
      <>
        <Row style={styles.row}>
          <Col><Text textHeader>Nota 1</Text></Col>
          <Col><Text textHeader>Nota 2</Text></Col>
          {sub ? <Col><Text textHeader>Sub.</Text></Col> : null}
          <Col><Text textHeader>Média</Text></Col>
        </Row>
        <Row style={styles.row}>
          <Col><Text textChild>{noteOne || '-'}</Text></Col>
          <Col><Text textChild>{noteTwo || '-'}</Text></Col>
          {sub ? <Col><Text textChild>{sub || '-'}</Text></Col> : null}
          <Col><Text textChild>{average || '-'}</Text></Col>
        </Row>
      </>
    );

    const gridMedicine = () => (
      <>
        <Row style={styles.row}>
          <Col><Text textHeader>Nota 1</Text></Col>
          <Col><Text textHeader>Nota 2</Text></Col>
          <Col><Text textHeader>Nota 3</Text></Col>
        </Row>
        <Row style={styles.row}>
          <Col><Text textChild>{noteOne || '-'}</Text></Col>
          <Col><Text textChild>{noteTwo || '-'}</Text></Col>
          <Col><Text textChild>{noteThree || '-'}</Text></Col>
        </Row>
        <Row style={styles.row}>
          <Col><Text textHeader>Nota 4</Text></Col>
          <Col><Text textHeader>Sub.</Text></Col>
          <Col><Text textHeader>Média</Text></Col>
        </Row>
        <Row style={styles.row}>
          <Col><Text textChild>{noteFour || '-'}</Text></Col>
          <Col><Text textChild>{sub || '-'}</Text></Col>
          <Col><Text textChild>{average || '-'}</Text></Col>
        </Row>
      </>
    );

    return (
      <View style={style}>
        <View author margedBottom>
          <Left>
            <Ball color={approved ? '#3497FD' : (cursed) ? '#3ACCE1' : 'red'} />
            <Text textInfo>{title}</Text>
          </Left>
        </View>

        <Grid>
          {isFromMedicine ? gridMedicine() : gridComum()}
          <Row style={styles.row}>
            <Col>
              <Text textHeader>Falta 1</Text>
            </Col>
            <Col>
              <Text textHeader>Falta 2</Text>
            </Col>
            <Col>
              <Text textHeader>Freq.</Text>
            </Col>
            {sub ? <Col /> : null}
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textChild>{faultsOne || '-'}</Text>
            </Col>
            <Col>
              <Text textChild>{faultsTwo || '-'}</Text>
            </Col>
            <Col>
              <Text textChild>{frequency || '0'}%</Text>
            </Col>
            {sub ? <Col /> : null}
          </Row>
        </Grid>
      </View>
    );
  }
}

export default connectStyle('Portal.NotesItem', styles, NotesItem);
