import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import { Item, Input as NBInput, Icon } from 'native-base';
import { omit } from 'ramda';

import styles from './styles';
import { connectStyle } from '../../utils';

const onlyInputProps = omit(['style']);

class Input extends PureComponent {
  static propTypes = {
    ...NBInput.propTypes,
  };

  render() {
    const { onPressIcon, iconRight } = this.props;

    let renderRight;
    if (iconRight) {
      renderRight = (
        <TouchableOpacity onPress={onPressIcon}>
          <Icon
            name={ iconRight || 'plus' }
            size={1}
          />
        </TouchableOpacity>
      );
    }
    
    return (
      <Item>
        <NBInput
          {...onlyInputProps(this.props)}
        />
        { renderRight }
      </Item>
    );
  }
}

export default connectStyle('Portal.Input', styles, Input);
