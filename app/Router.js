import React, { Fragment } from 'react';
import { StatusBar } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { setContainer } from './services';
import {
  Home,
  Notes,
  Login,
  Semesters,
  Finance,
  Tickets,
  LibrarySearch,
  LibraryListMaterials,
  LibraryMaterial,
  ContactUs,
} from './containers';

const RootStack = createStackNavigator({
  ContactUs: {
    screen: ContactUs,
  },
  Home: {
    screen: Home,
  },
  Notes: {
    screen: Notes,
  },
  Semesters: {
    screen: Semesters,
  },
  Login: {
    screen: Login,
  },
  Finance: {
    screen: Finance,
  },
  Tickets: {
    screen: Tickets,
  },
  LibrarySearch: {
    screen: LibrarySearch,
  },
  LibraryListMaterials: {
    screen: LibraryListMaterials,
  },
  LibraryMaterial: {
    screen: LibraryMaterial,
  },
}, {
  headerMode: 'none',
  initialRouteName: 'Login',
});

const Routers = createAppContainer(RootStack);

const Router = () => (
  <Fragment>
    <StatusBar  
      backgroundColor="#FFF"
      barStyle="dark-content"
    />
    <Routers ref={setContainer} />
  </Fragment>
);

export default Router;