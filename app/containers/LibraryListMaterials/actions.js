import { toSoapArgs, createEnvelope, headers } from '../../boot/config';
import { alert } from '../../utils';
import { navigate } from '../../services';
import types from './types';

export function getInfosMaterial(material) {
  return (dispatch) => {
    dispatch({
      type: types.GET_DATA,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/getTitulo'),
          data: createEnvelope('getTitulo', toSoapArgs([material.id])),
        },
      },
    }).then(async () => {
      try {
        dispatch(getAuthor(material.id));
        dispatch(getAssunts(material.id));
        dispatch(getExamples(material.id));
        navigate('LibraryMaterial');
      } catch (err) {
        console.log('error', err);
        return alert({
          title: 'Portal Uni-Facef',
          text: 'Opa! Não foi possível resgatar as informações neste momento. Tente novamente, por favor.'
        });
      }
    })
    .catch((err) => {
      console.log('error', err);
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não foi possível resgatar as informações neste momento. Tente novamente, por favor.'
      });
    });
  };
}

export function setCurrentMaterial(material) {
  return {
    type: types.SET_MATERIAL,
    material,
  }
}

export function getAuthor(idMaterial) {
  return (dispatch) => {
    dispatch({
      type: types.GET_AUTHORS,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/localizarAutor'),
          data: createEnvelope('localizarAutor', toSoapArgs([idMaterial])),
        },
      },
    });
  };
}

export function getAssunts(idMaterial) {
  return (dispatch) => {
    dispatch({
      type: types.GET_ASSUNTS,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/localizarAssunto'),
          data: createEnvelope('localizarAssunto', toSoapArgs([idMaterial])),
        },
      },
    });
  };
}

export function getExamples(idMaterial) {
  return (dispatch) => {
    dispatch({
      type: types.GET_EXAMPLES,
      payload: {
        request: {
          url: 'BibliotecaWS/BibliotecaWS',
          method: 'POST',
          headers: headers('BibliotecaWS/localizarExemplar'),
          data: createEnvelope('localizarExemplar', toSoapArgs([idMaterial])),
        },
      },
    });
  };
}