// @flow

import variable from './../';
import { em } from '../../utils';

export default (variables /*: * */ = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: 'Gibson',
    includeFontPadding: false,
    color: variables.textColor,
    '.note': {
      color: '#a7a7a7',
      fontSize: variables.noteFontSize
    },
    '.textCard':{
      position: 'absolute',
      color: '#fff',
    },
    '.textHeader': {
      fontSize: em(1.2),
      fontWeight: '400',
      color: '#454F63',
      alignSelf: 'center',
    },
    '.textChild': {
      fontWeight: '400',
      alignSelf: 'center',
      color: '#78849E',
    },
    '.titleCard': {
      bottom: 33,
      fontSize: em(2),
      left: 10,
    },
    '.textInfo': {
      color: '#78849E',
      fontWeight: '400',
      paddingRight: 10,
    },
    '.marginTop': {
      marginTop: 15,
    },
    '.textTitle': {
      fontSize: em(1.3),
      color: '#454F63',
    },
    '.subTitleCard': {
      bottom: 12,
      opacity: 0.75,
      fontSize: em(1.5),
      left: 11,
    },
  };

  return textTheme;
};
