import { Platform } from 'react-native';

export default {
  row: {
    marginVertical: 5,
    marginLeft: Platform.OS == 'ios' ? -10 : -16,
  },
  grid: {
    marginTop: 15,
  },
};
