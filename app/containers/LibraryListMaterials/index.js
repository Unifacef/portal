import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content } from 'native-base';

import { Spinner, ContainerList } from '../../components';
import Materials from './components/Materials';
import * as actions from './actions';
import { toCapitalize } from '../../utils';

class LibraryListMaterials extends Component {
  onSelectedExample = (exampleId) => {
    this.props.setCurrentMaterial(exampleId);
    this.props.getInfosMaterial(exampleId);
  }

  render() {
    if (this.props.isLoadingSearch || this.props.isLoading) {
      return <Spinner />;
    }

    return (
      <ContainerList
        renderHeader
        title={toCapitalize(this.props.materialType)}
      >
        <Content padder>
          <Materials
            data={this.props.examples}
            onPress={this.onSelectedExample}
            materialType={this.props.materialType}
          />
        </Content>
      </ContainerList>
    );
  }
}

const mapStateToProps = ({ librarySearch, libraryMaterials }) => ({
  examples: librarySearch.examples,
  materialType: librarySearch.materialType,
  isLoading: libraryMaterials.isLoading,
  isLoadingSearch: librarySearch.isLoading,
});

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(LibraryListMaterials);
