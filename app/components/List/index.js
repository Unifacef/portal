import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Item as NBList, Right, Left, Text } from 'native-base';

import { Icon } from '../';
import styles from './styles';
import { connectStyle, computeProps, em } from '../../utils';

class List extends Component {
  render() {
    const { style } = computeProps(this.props);
    const {
      text,
      icon,
      onPress,
      color,
      size,
      leftIcon,
      noMargin,
      noRightIcon,
      isTheLast,
    } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ marginHorizontal: noMargin ? 0 : 8 }}
        {...this.props}
      >
        <NBList
          onPress={onPress}
          style={[style, { borderBottomColor: !!isTheLast ? 'transparent' : 'rgba(120, 132, 158, 0.2)' }]}
          leftIcon={!!leftIcon}
        >
          <Left>
            {leftIcon ? <Icon name={leftIcon} inRow /> : null}
            <Text numberOfLines={1}>{ text || '1º Semestre' }</Text>
          </Left>
          { noRightIcon ?
            null
            : <Right>
                <Icon
                  name={icon || 'chevron-right'}
                  size={size || em(1.6)}
                  color={color || false}
                />
              </Right>
          }
        </NBList>
      </TouchableOpacity>
    );
  }
}

export default connectStyle('Portal.List', styles, List);
