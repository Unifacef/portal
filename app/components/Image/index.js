import React, { PureComponent } from 'react';
import { Spinner } from 'native-base';
import { createImageProgress } from 'react-native-image-progress';
import FastImage from 'react-native-fast-image';

import { computeProps, connectStyle } from '../../utils';

const ImageComponent = createImageProgress(FastImage);

class Image extends PureComponent {
  render() {
    const { style } = computeProps(this.props);

    return (
      <ImageComponent
        {...this.props}
        onLoadEnd={() => {}}
        indicator={Spinner}
        indicatorProps={{ size: 'small' }}
        style={style}
        imageStyle={style}
      />
    );
  }
}

export default connectStyle('Aulapp.Image', {}, Image);
