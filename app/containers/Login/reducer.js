import { handleActions, combineActions } from 'redux-actions';
import types from './types';

const INITIAL_STATE = {
  code: '',
  password: '',
  user: [],
  token: {},
  isLoading: true,
  userActual: {},
};

const beginLoading = combineActions(types.AUTH_REQUEST, types.AUTH_GET_USER);

const stopLoading = combineActions(
  types.AUTH_REQUEST_SUCCESS,
  types.AUTH_REQUEST_FAIL,
  types.AUTH_GET_USER_SUCCESS,
  types.AUTH_GET_USER_FAIL,
  types.AUTH_STOP_LOADING,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),    
    [types.SET_VALUE]: (state,  { input, value }) => ({
      ...state,
      [input]: value,
    }),
    [types.AUTH_SET_TOKEN]: (state, { token }) => ({
      ...state,
      token,
    }),
    [types.AUTH_SET_USER]: (state, { user }) => ({
      ...state,
      user: user.map(userMap => ({
        ...userMap,
        ano: userMap.serie.slice(0, 4),
      })),
      userActual: user[user.length - 1],
    }),
    [types.LOGOUT]: () => ({ ...INITIAL_STATE }),
  },
  INITIAL_STATE,
);

export default reducer;
