export default {
  flex: 1,
  borderRadius: 12,
  borderColor: 'transparent',
  'Portal.Card': {
    height: 156,
    marginHorizontal: 8,
    borderColor: 'transparent',
  },
};
