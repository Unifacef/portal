export default (string) => {
  const stringOnArray = string.split(' ');
  const splitString = stringOnArray.map((item, index) => {
    const previousWord = stringOnArray[index - 1] ? stringOnArray[index - 1] : '';
    if ((item === 'II') || (item === 'I')) {
      return item;
    }

    if (previousWord.charAt(previousWord.length - 1) === ':') {
      return item.charAt(0).toUpperCase() + item.slice(1).toLowerCase();
    }

    if ((item.length === 2 || item.length === 1) && index !== 0) {
      return item.toLowerCase();
    }

    return item.charAt(0).toUpperCase() + item.slice(1).toLowerCase();
  });
  
  const joinString = splitString.join(' ');
  return joinString.replace('º', '°');
}