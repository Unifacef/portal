import React, { Component } from 'react';
import { Container } from 'native-base';
import { ScrollView } from 'react-native';

import { Header } from '../';

class ContainerList extends Component {
  render() {
    const { title, renderHeader, onPressLeft } = this.props;

    return (
      <Container gray>
        <Header
          renderHeader={renderHeader}
          title={title}
          onPressLeft={onPressLeft || false}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
        >
          { this.props.children }
        </ScrollView>
      </Container>
    );
  }
}

export default ContainerList;
