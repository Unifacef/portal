import { em } from '../../utils';
import variable from './../';

export default (variables /*: * */ = variable) => {
  const bodyTheme = {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    '.padder': {
      paddingHorizontal: 15,
      paddingVertical: 15,
    },
    '.padderBottom': {
      paddingBottom: '20%',
    },
  };

  return bodyTheme;
};
