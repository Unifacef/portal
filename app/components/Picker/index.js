import React, { PureComponent } from 'react';
import { Picker as NBPicker } from 'native-base';
import { Platform } from 'react-native';
import { isPlainObject, noop } from 'lodash';

import { Header } from '../';
import styles from './styles';
import { connectStyle, computeProps } from '../../utils';

const Item = NBPicker.Item;

class Picker extends PureComponent {
  render() {
    const { style } = computeProps(this.props);
    const { options, onChange, title, placeholder, selected } = this.props;
    const propsOnChange = onChange || noop;
    const propsOptions = options || [];

    return (
      <NBPicker
        placeholder={
          Platform.select({
            android: (placeholder || 'Selecione...').toUpperCase(),
            ios: placeholder || 'Selecione...',
          })
        }
        renderHeader={(back) => (
          <Header
            renderHeader
            onPressLeft={back}
            title={title}
          />
        )}
        mode="dropdown"
        style={style}
        textStyle={{
          alignSelf: 'center',
          color: '#454F63',
        }}
        selectedValue={selected}
        onValueChange={propsOnChange}
      >
        {propsOptions.map((item, n) => {
          if (isPlainObject(item)) {
            return <Item {...item} key={n} />;
          }
  
          return <Item label={item} value={item} key={item} />;
        })}
      </NBPicker>
    );
  }
};

export default connectStyle('Portal.Picker', styles, Picker);
