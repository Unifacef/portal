import React, { Component } from 'react';

import { Container, Header } from '../../components';
import Infos from './components/Tabs';

class ContactUs extends Component {
  render() {
    return (
      <Container gray>
        <Header renderHeader title="Informações" noShadow />
        <Infos />
      </Container>
    );
  }
}

export default ContactUs;
