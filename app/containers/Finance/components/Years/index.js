import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import { Content } from 'native-base';

import { List } from '../../../../components';

class Years extends PureComponent {
  _keyExtractor = (item) => item.id;

  render() {
    const { years, onPressYear } = this.props;

    return (
      <Content padder>
        <FlatList
          data={years}
          keyExtractor={this._keyExtractor}
          renderItem={({item}) => {
            return (
              <List
                key={item.id}
                onPress={() => onPressYear(item.id, item.serie.slice(0, 4))}
                text={item.serie.slice(0, 4)}
              />
            );
          }}
        />
      </Content>
    );
  }
}

export default Years;
