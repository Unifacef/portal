import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { View, Left, Text, Content, Container } from 'native-base';

import { toCapitalize } from '../../../../utils';
import { Card } from '../../../../components';

class Assunts extends Component {
  _keyExtractor = (item, index) => index.toString();

  render() {
    const { assunts } = this.props;

    return (
      <Container gray>
        <Content 
          showsVerticalScrollIndicator={false}
          margedBottom
          padderTop
          margedHorizontal
        >
          <FlatList
            data={assunts}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => (
              <Card key={index}>
                <View assunts>
                  <Left>
                    <Text>{ toCapitalize(item.nome) }</Text>
                  </Left>
                </View>
              </Card>
            )}
          />
        </Content>
      </Container>
    );
  }
}

export default Assunts;
