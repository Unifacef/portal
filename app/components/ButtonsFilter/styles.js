import { Platform, Dimensions } from 'react-native';
import { em } from '../../utils';

const { width } = Dimensions.get('window');

export default {
  modalCalendar: {
    maxHeight: ((width - em(1.25) * 2) * (3 / 8)) + 160,
    width:  (width - em(1.25) * 2) - 50,
    borderRadius: 14,
    padding: 0,
    margin: 0,
  },
  modalDiscipline: {
    height: ((width - em(1.25) * 2) * (3 / 8)) + 210,
    width:  (width - em(1.25) * 2) - 30,
    paddingVertical: 10,
    borderRadius: 14,
    padding: 0,
    margin: 0,
  },
  'NativeBase.ViewNB': {
    width: '41%',
    borderRadius: 14,
    height: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.84,
    elevation: 3,
    backgroundColor: '#353A50',
    alignItems: 'flex-start',
    'NativeBase.Left': {
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: 10,
      'NativeBase.Text': {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: Platform.OS === 'ios' ? em(1.1) : em(1.4),
        fontFamily: 'Gibson',
        color: '#fff',
      },
    },
  },
};
