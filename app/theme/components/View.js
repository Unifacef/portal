// @flow

import variable from './../';
import { em } from '../../utils';

export default (variables /*: * */ = variable) => {
  const viewTheme = {
    '.padder': {
      padding: variables.contentPadding
    },
    '.padderAll': {
      paddingHorizontal: 10,
      paddingVertical: 15,
    },
    '.author': {
      width: '100%',
      height: 'auto',
      flexDirection: 'row',
    },
    '.margedTop': {
      marginTop: 15,
    },
    '.margedBottom': {
      marginBottom: 15,
    },
    '.general': {
      width: '100%',
      height: 80,
      flexDirection: 'row',
      marginTop: 5,
      borderBottomColor: 'rgba(120, 132, 158, 0.5)',
      marginBottom: 7,
      'NativeBase.Left': {
        'NativeBase.Text': {
          marginBottom: 3,
        },
      },
    },
    '.assunts': {
      width: '100%',
      height: 60,
      flexDirection: 'row',
      paddingHorizontal: 15,
      'NativeBase.Left': {
        'NativeBase.Text': {
          fontSize: em(1.3),
          fontFamily: 'Gibson',
          color: '#454F63',
        },
      },
    },
    '.collapsableView': {
      width: '100%',
      height: 60,
      flexDirection: 'row',
      paddingHorizontal: 15,
      marginHorizontal: 2,
      marginVertical: 5,
      'NativeBase.Left': {
        flex: 80,
        flexDirection: 'row',
        alignItems: 'center',
        'NativeBase.Text': {
          fontSize: em(1.3),
          fontFamily: 'Gibson',
          color: '#454F63',
          marginLeft: 10,
        },
      },
      'NativeBase.Right': {
        flex: 20,
      },
    }
  };

  return viewTheme;
};
