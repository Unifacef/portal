import React, { PureComponent } from 'react';
import { Spinner as NBSpinner, View } from 'native-base';

import { connectStyle, computeProps } from '../../utils';
import styles from './styles';

class Spinner extends PureComponent {
  render() {
    const { style } = computeProps(this.props);
    const { color } = this.props;

    return (
      <View style={style}>
        <NBSpinner color={ color || '#454F63' } />
      </View>
    )
  }
}

export default connectStyle('Portal.Spinner', styles, Spinner);
