import axios from 'react-native-axios';

const DEVELOP_HOST = 'http://sga.unifacef.com.br/';

export default {
  default: {
    client: axios.create({
      baseURL: DEVELOP_HOST,
      responseType: 'json',
    }),
    options: {
      returnRejectedPromiseOnError: true,
      response: [
        {
          success: (store, response) => response,
          error: (store, error) => {
            if (error.response && error.response.status === 401) {
              return AsyncStorage.multiRemove(['@minerva/user', '@minerva/token'])
                .then(() => reset('Login'));
            }
            console.log('error', error);
            return Promise.reject(error);
          }
        }
      ]
    }
  },
};
