import React, { Component } from 'react';
import { View, Left, Text, Content, Container } from 'native-base';

import { Card } from '../../../../components';

class Units extends Component {
  render() {
    return (
      <Container gray>
        <Content
          showsVerticalScrollIndicator={false}
          margedBottom
          padderTop
          margedHorizontal
        >
          <Card margedHorizontal padderAll>
            <Text textTitle>Unidade I</Text>
            <View author><Left><Text textInfo>Av. Major Nicácio, 2433</Text></Left></View>
            <View author><Left><Text textInfo>Bairro São José</Text></Left></View>
            <View author><Left><Text textInfo>CEP 14401-135</Text></Left></View>
          </Card>
          <Card margedHorizontal padderAll>
            <Text textTitle>Unidade II e III</Text>
            <View author><Left><Text textInfo>Av. Dr. Ismael Alonso y Alonso, 2400</Text></Left></View>
            <View author><Left><Text textInfo>Bairro São José</Text></Left></View>
            <View author><Left><Text textInfo>CEP 14403-430</Text></Left></View>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default Units;
