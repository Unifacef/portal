import { Linking } from 'react-native';
import { toSoapArgs, createEnvelope, headers, xmlToJSON } from '../../boot/config';
import { alert } from '../../utils';
import { navigate } from '../../services';
import types from './types';

export function setCurrentSemester(semester) {
  return (dispatch, getState) => {
    const { user } = getState().login;
    const removeProvavel = user.filter(item => item.situacao != "PROVAVEL FORMANDO");
    if (semester === removeProvavel[removeProvavel.length - 1].id) return dispatch(verifyQuestionary(semester));
    dispatch({ type: types.SET_CURRENT_SEMESTER, currentSemester: semester });
    return navigate('Notes');
  };
}
export function setCurrentsDisciplines(disciplines) {
  return {
    type: types.SET_CURRENTS_DISCIPLINES,
    disciplines,
  };
}

export function verifyQuestionary(semester) {
  return (dispatch, getState) => {
    const { code, user } = getState().login;
    const removeProvavel = user.filter(item => item.situacao != "PROVAVEL FORMANDO");

    dispatch({
      type: types.VERIFY_QUESTIONARY,
      payload: {
        request: {
          url: 'NotaWS/NotaWS',
          method: 'POST',
          headers: headers('NotaWS/avaliacaoRespondida'),
          data: createEnvelope('avaliacaoRespondida', toSoapArgs([code || removeProvavel[removeProvavel.length - 1].pessoa])),
        },
      },
    }).then(({ payload: { data } }) => {
      const result = JSON.parse(xmlToJSON(data, 'avaliacaoRespondida'));
      if (!result) {
        return alert({
          title: 'Portal Uni-Facef',
          text: 'Opa! Parece que você não respondeu ao questionário. Responder?',
          yesOrNo: true,
          yes: () => Linking.openURL('http://avalinst.unifacef.com.br', '_system', 'location=yes'),
        });
      }
      dispatch({ type: types.SET_CURRENT_SEMESTER, currentSemester: semester });
      return navigate('Notes');
    }).catch(() => {
      return alert({
        title: 'Portal Uni-Facef',
        text: 'Opa! Não conseguimos ver se você já respodeu ao questionário neste momento. Tente novamente, por favor.',
      });
    });
  }
}