import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Spinner, Container, Header } from '../../components';
import TabsMaterial from './components/TabsMaterial';

class LibraryMaterial extends Component {
  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    }

    return (
      <Container gray>
        <Header
          renderHeader
          title="Especificações"
          noShadow
        />
        <TabsMaterial
          currentData={this.props.currentData}
          currentAuthors={this.props.currentAuthors}
          currentAssunts={this.props.currentAssunts}
          currentExamples={this.props.currentExamples}
          material={this.props.material}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ libraryMaterials }) => ({
  material: libraryMaterials.material,
  isLoading: libraryMaterials.isLoading,
  currentData: libraryMaterials.currentData,
  currentAuthors: libraryMaterials.currentAuthors,
  currentAssunts: libraryMaterials.currentAssunts,
  currentExamples: libraryMaterials.currentExamples,
});

export default connect(mapStateToProps, {})(LibraryMaterial);
