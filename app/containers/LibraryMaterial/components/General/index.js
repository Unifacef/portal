import React, { Component } from 'react';
import { View, Left, Text, Content, Grid, Col, Row } from 'native-base';

import { If, toCapitalize } from '../../../../utils';
import { Card, Container } from '../../../../components';
import styles from './styles';

class General extends Component {
  render() {
    const { material, authors, data } = this.props;
    const qtdeDisponivel = Number(material.valor2).toFixed(0);

    return (
      <Container gray>
        <Content padder>
          <Card margedHorizontal padderAll>
            <Text textTitle>Título</Text>
            <View author>
              <Left>
                <Text textInfo>{toCapitalize(material.material || '')}</Text>
              </Left>
            </View>
            <Text textTitle marginTop>
              Autor
            </Text>
            <View author>
              <Left>
                <If condition={Array.isArray(authors)}>
                  {() => authors.map(author => <Text textInfo>{toCapitalize(author.nome || '')}</Text>)} 
                </If>
                <If condition={typeof authors == 'object'}>
                  {() => <Text textInfo>{toCapitalize(authors.nome || '')}</Text>}
                </If>
              </Left>
            </View>
            <Grid style={styles.grid}>
              <Row style={styles.row}>
                <Col>
                  <Text textHeader>{qtdeDisponivel === '1' ? 'Disponível' : 'Disponíveis'}</Text>
                </Col>
                <Col>
                  <Text textHeader>Qtde.</Text>
                </Col>
                <Col>
                  <Text textHeader>Referência</Text>
                </Col>
              </Row>
              <Row style={styles.row}>
                <Col>
                  <Text textChild>{qtdeDisponivel}</Text>
                </Col>
                <Col>
                  <Text textChild>{Number(material.valor1).toFixed(0)}</Text>
                </Col>
                <Col>
                  <Text textChild>{`${data[0]} - ${data[1]}`}</Text>
                </Col>
              </Row>
            </Grid>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default General;
