export default {
  'NativeBase.ViewNB': {
    borderBottomColor: 'rgba(120, 132, 158, 0.5)',
    marginHorizontal: 6,
    marginVertical: 15,
    '.last': {
      borderBottomWidth: 0,
    },
    'NativeBase.ViewNB': {
      'NativeBase.Left': {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-start',
        'NativeBase.Text': {
          marginHorizontal: 5,
        },
      },
    },
  },
  row: {
    marginVertical: 5,
  },
};
