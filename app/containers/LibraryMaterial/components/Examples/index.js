import React, { Component } from 'react';
import { Content } from 'native-base';

import { Container, ExamplesItem } from '../../../../components';;

class Examples extends Component {
  renderExamples = (item, index) => (
    <ExamplesItem
      key={index}
      publishingCompany={item.material}
      year={item.documento}
      situation={item.situacao}
      edit={item.lote}
      id={item.id}
      volume={item.serie}
    />
  );

  render() {
    const { examples } = this.props;

    return (
      <Container gray>
        <Content padder>
          {examples.map((material, i) => {
            if (Array.isArray(material))
              return material.map((item, index) => this.renderExamples(item, index));

            return this.renderExamples(material, i);
          })}
        </Content>
      </Container>
    );
  }
}

export default Examples;
