import X2JS from './x2js';

const x2js = new X2JS();

const toSoapArgs = (params) => params.map((item, index) => `<arg${index}>${item}</arg${index}>`).join('');

const createEnvelope = (method, body) => (`
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Header/>
  <S:Body>
    <ns2:${method} xmlns:ns2="http://ws.education.eddydata.com.br/">
      ${body}
    </ns2:${method} >
  </S:Body>
</S:Envelope>
`);

const headers = (urlMethod) => ({
  'Content-Type': 'text/xml; charset=utf-8',
  SOAPAction: `http://sga.unifacef.com.br/${urlMethod}`,
});

const getIsMatriculaValidaReturn = xml => Number(/<return>([0-9]+)<\/return>/gi.exec(xml)[1]);

const xmlToJSON = (xmlData, method) => x2js.xml2js(xmlData).Envelope.Body[`${method}Response`].return;

export {
  toSoapArgs,
  createEnvelope,
  headers,
  getIsMatriculaValidaReturn,
  xmlToJSON,
};

export default {
  toSoapArgs,
  createEnvelope,
  headers,
  getIsMatriculaValidaReturn,
  xmlToJSON,
};
