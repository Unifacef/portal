import { em } from '../../utils';

export default {
  height: 70,
  marginHorizontal: 8,
  '.isCollapsible': {
    marginHorizontal: 7.5,
    paddingHorizontal: 1,
    marginVertical: 5,
  },
  'NativeBase.Item': {
    '.leftIcon': {
      'NativeBase.Left': {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 80,
        'NativeBase.Text': {
          fontFamily: 'Gibson',
          fontSize: em(1.2),
          color: '#454F63',
          marginLeft: 10,
        },
      },
      'NativeBase.Right': {
        flex: 20,
      },
    },
    'NativeBase.Left': {
      'NativeBase.Text': {
        fontFamily: 'Gibson',
        fontSize: em(1.2),
        color: '#454F63',
      },
    },
  },
};
