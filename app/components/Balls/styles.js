import { em } from '../../utils';

export default {
  'NativeBase.ViewNB': {
    flexDirection: 'column',
    'NativeBase.ViewNB': {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      marginTop: 8,
      marginBottom: 8,
      'NativeBase.ViewNB': {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 1,
        'NativeBase.Text': {
          color: '#78849E',
          fontFamily: 'Gibson',
          fontSize: em(1),
          fontWeight: '300',
        },
      },
    },
  },
};
