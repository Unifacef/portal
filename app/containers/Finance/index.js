import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, flatten, prop, uniqBy } from 'ramda';

import { ContainerList } from '../../components';
import Years from './components/Years';
import * as actions from './actions';
import { getTickets } from '../Tickets/actions';
import { navigate } from '../../services';

const getYears = compose(uniqBy(prop('ano')), flatten);

class Finance extends Component {
  onPressYear = (semester, year) => {
    this.props.setCurrentYear(semester, year);
    this.props.getTickets(semester);
    navigate('Tickets');
  }

  render() {
    return (
      <ContainerList renderHeader title="Financeiro">
        <Years
          years={getYears(this.props.user)}
          onPressYear={this.onPressYear}
        />
      </ContainerList>
    );
  }
}

const mapStateToProps = state => ({
  user: state.login.user,
});

const mapDispatchToProps = {
  ...actions,
  getTickets,
};

export default connect(mapStateToProps, mapDispatchToProps)(Finance);
