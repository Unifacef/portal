import { em } from '../../utils';

export default {
 '.inRow': {
    fontSize: em(1.2),
    marginTop: 3,
 },
};
