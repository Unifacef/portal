import { types, async } from '../../utils/typeCreator';

export default types([
  ...async('AUTH_REQUEST'),
  ...async('AUTH_GET_USER'),
  'SET_VALUE',
  'AUTH_SET_TOKEN',
  'LOGOUT',
  'AUTH_STOP_LOADING',
  'AUTH_SET_USER',
], 'LOGIN');
