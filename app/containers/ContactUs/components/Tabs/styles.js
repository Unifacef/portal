import em from '../../../../utils/em';

export default {
  tabs: {
    backgroundColor: '#fff',
  },
  tab: {
    backgroundColor: '#fff',
  },
  textTab: {
    fontSize: em(1.1),
    fontWeight: '300',
    color: '#454F63',
    fontFamily: 'Gibson',
  },
};
