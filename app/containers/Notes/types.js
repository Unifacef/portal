import { types, async } from '../../utils/typeCreator';

export default types([
  ...async('GET_NOTES'),
], 'NOTES');
