export default {
  image: {
    width: '80%',
    height: 100,
    paddingHorizontal: 60,
    resizeMode: 'contain',
    marginVertical: 40,
  },
  alignItems: 'center',
  justifyContent: 'center',
  flex: 10,
  height: '100%',
  marginHorizontal: 12,
};
