import types from './types';

export function setCurrentYear(semester, year) {
  return {
    type: types.SET_CURRENT_YEAR,
    currentSemester: semester,
    currentYear: year,
  };
}
