// @flow

import variable from "./../";

export default (variables /*: * */ = variable) => {
  const switchTheme = {
    marginVertical: -5,
  };

  return switchTheme;
};
