import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getNotes } from '../Notes/actions';
import SemestersList from './components/SemestersList';
import { ContainerList } from '../../components';
import * as actions from './actions';

class Semesters extends Component {
  onPressSemester = (semester) => {
    this.props.setCurrentSemester(semester);
    this.props.getNotes(semester);
  }

  render() {
    return (
      <ContainerList
        renderHeader
        title="Semestres"
        onPressLeft={this.onPressBack}
      >
        <SemestersList
          semesters={this.props.user}
          onPressSemester={this.onPressSemester}
        />
      </ContainerList>
    );
  }
}

const mapStateToProps = ({ login, semesters }) => ({
  isLoading: semesters.isLoading,
  user: login.user,
  isQuestionaryAnswered: semesters.isQuestionaryAnswered,
});

const mapDispatchToProps = {
  ...actions,
  getNotes,
}

export default connect(mapStateToProps, mapDispatchToProps)(Semesters);
