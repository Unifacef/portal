import React, { Component } from 'react';
import { TouchableOpacity, ScrollView } from 'react-native';
import { View, Text, Left } from 'native-base';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Modal from 'react-native-modalbox';
import moment from 'moment';

import styles from './styles';
import { ListButton, Icon } from '../';
import { em, splitString, connectStyle, computeProps } from '../../utils';

LocaleConfig.locales['pt-br'] = {
  monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
  monthNamesShort: ['Jan.', 'Fev.', 'Mar.', 'Abr.', 'Maio', 'Jun.', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
  dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
  dayNamesShort: ['Dom.', 'Seg.', 'Ter.', 'Qua.', 'Qui.', 'Sex.', 'Sáb.'],
};

LocaleConfig.defaultLocale = 'pt-br';

class ButtonsFilter extends Component {
  selectDay = (selectedDate) => {
    this.props.onPress(selectedDate.dateString);
    setTimeout(() => {this.refs.date.close()}, 100);
  }

  selectDiscipline = (selectedDiscipline) => {
    this.props.onPress(selectedDiscipline);
    setTimeout(() => {this.refs.discipline.close()}, 100);
  }

  render() {
    const { style } = computeProps(this.props);
    const { text, icon, date, data } = this.props;

    return (
      <View style={style}>
        <TouchableOpacity onPress={() => date ? this.refs.date.open() : this.refs.discipline.open()}>
          <Left>
            <Icon
              name={ icon || 'calendar' }
              size={em(1.5)}
              color="#676B7C"
            />
            <Text>
              { text.length < 22 ? (text.length === 5) ? text : splitString(text) : `${text.substring(0,16)}...` }
            </Text>
          </Left>
        </TouchableOpacity>
        <Modal
          style={styles.modalCalendar}
          ref="date"
          coverScreen
          position="center"
        >
          <Calendar
            current={moment().format('YYYY-MM-DD')}
            minDate={moment().format('YYYY')}
            onDayPress={day => this.selectDay(day)}
            monthFormat="MMMM, yyyy"
            hideArrows={false}
            hideExtraDays
          />
        </Modal>
        <Modal
          position="center"
          coverScreen
          style={styles.modalDiscipline}
          ref="discipline"
        >
          <ScrollView
            showsVerticalScrollIndicator={false}
          >
            <ListButton
              data={data}
              onPress={this.selectDiscipline}
              itemSelected={text}
            />
          </ScrollView>
        </Modal>
      </View>
    );
  }
}

export default connectStyle('Portal.ButtonsFilter', styles, ButtonsFilter);
