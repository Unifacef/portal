import React, { PureComponent } from 'react';
import { FlatList, Keyboard } from 'react-native';
import { Right, Text, View, Left } from 'native-base';
import NBCollapsible from 'react-native-collapsible';

import { toCapitalize, connectStyle, computeProps, selectIcon, em } from '../../utils';
import { List, Icon, Card } from '../';

class Collapsible extends PureComponent {
  state = {
    isCollapsed: true,
  };

  _keyExtractor = (item) => item.id;

  handleCollapsible = () => {
    Keyboard.dismiss();
    return this.setState({ isCollapsed: !this.state.isCollapsed });
  }

  onSelectItem = (item) => {
    this.handleCollapsible();
    return this.props.onPress(item);
  }

  render() {
    const { style } = computeProps(this.props);
    const { data, value, text } = this.props;

    return (
      <Card
        onPress={this.handleCollapsible}
        style={{ maxHeight: text ? undefined : 761 }}
        style={style}
      >
        <View collapsableView>
          <Left>
            {value ? <Icon inRow name={selectIcon(value)} /> : null}
            <Text numberOfLines={1}>
              {toCapitalize(value)}
            </Text>    
          </Left>
          <Right>
            <Icon
              name={this.state.isCollapsed ? 'chevron-down' : 'chevron-up'}
              size={em(1.3)}
              onPress={this.handleCollapsible}
            />
          </Right>
        </View>
        <NBCollapsible collapsed={this.state.isCollapsed}>
          <FlatList
            data={data}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => {
              return (
                <List
                  isCollapsible
                  leftIcon={selectIcon(item.name)}
                  isTheLast={index == data.length - 1}
                  key={index}
                  noMargin
                  noRightIcon
                  onPress={() => this.onSelectItem(item.name)}
                  text={toCapitalize(item.name)}
                />
              );
            }}
          />
        </NBCollapsible>
      </Card>
    );
  }
}

export default connectStyle('Portal.Collapsible', {}, Collapsible);
