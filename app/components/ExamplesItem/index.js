import React, { Component } from 'react';
import { View, Left, Text, Col, Row, Grid } from 'native-base';

import { connectStyle, toCapitalize } from '../../utils';
import { Card } from '../';
import styles from './styles';

class ExamplesItem extends Component {
  render() {
    const {
      publishingCompany,
      year,
      situation,
      edit,
      id,
      volume,
    } = this.props;

    return (
      <Card margedHorizontal padderAll>
        <Text textTitle>
          Editora
        </Text>
        <View author margedBottom>
          <Left>
            <Text textInfo>{toCapitalize(publishingCompany)}</Text>
          </Left>
        </View>
        <Grid>
          <Row style={styles.row}>
            <Col>
              <Text textHeader>Tombo</Text>
            </Col>
            <Col>
              <Text textHeader>Situação</Text>
            </Col>
            <Col>
              <Text textHeader>Ano</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textChild>{ id }</Text>
            </Col>
            <Col>
              <Text textChild>{ situation }</Text>
            </Col>
            <Col>
              <Text textChild>{ year }</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textHeader>Edição</Text>
            </Col>
            <Col>
              <Text textHeader>Volume</Text>
            </Col>
            <Col /> 
          </Row>
          <Row style={styles.row}>
            <Col>
              <Text textChild>{ edit }</Text>
            </Col>
            <Col>
              <Text textChild>{ volume }</Text>
            </Col>
            <Col />
          </Row>
        </Grid>
      </Card>
    );
  }
}

export default connectStyle('Portal.ExamplesItem', styles, ExamplesItem);
