import { handleActions, combineActions } from 'redux-actions';
import { xmlToJSON } from '../../boot/config';
import types from './types';

const INITIAL_STATE = {
  isLoading: false,
  material: {},
  currentData: null,
  currentAuthors: [],
  currentAssunts: [],
  currentExamples: [],
};

const beginLoading = combineActions(
  types.GET_DATA,
  types.GET_AUTHORS,
  types.GET_ASSUNTS,
  types.GET_EXAMPLES,
);

const stopLoading = combineActions(
  types.GET_EXAMPLES_SUCCESS,
  types.GET_EXAMPLES_FAIL,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),
    [types.SET_MATERIAL]: (state, { material }) => ({
      ...state,
      material,
    }),
    [types.GET_ASSUNTS_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      currentAssunts: xmlToJSON(data, 'localizarAssunto'),
    }),
    [types.GET_EXAMPLES_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      currentExamples: xmlToJSON(data, 'localizarExemplar'),
    }),
    [types.GET_AUTHORS_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      currentAuthors: xmlToJSON(data, 'localizarAutor'),
    }),
    [types.GET_DATA_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      currentData: xmlToJSON(data, 'getTitulo'),
    }),
  },
  INITIAL_STATE,
);

export default reducer;
