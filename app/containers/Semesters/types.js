import { types, async } from '../../utils/typeCreator';

export default types([
  'SET_CURRENT_SEMESTER',
  'SET_CURRENTS_DISCIPLINES',
  ...async('VERIFY_QUESTIONARY'),
], 'SEMESTERS');
