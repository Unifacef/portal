import { types, async } from '../../utils/typeCreator';

export default types([
  ...async('MAKE_SEARCH'),
  ...async('GET_ALL_TYPES'),
  'SET_MATERIAL_TYPE',
  'SET_VALUE',
  'CLEAR_STATE',
], 'LIBRARY_SEARCH');
