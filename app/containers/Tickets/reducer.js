import { handleActions, combineActions } from 'redux-actions';
import { xmlToJSON } from '../../boot/config';
import types from './types';

const INITIAL_STATE = {
  isLoading: true,
  tickets: [],
};

const beginLoading = combineActions(types.GET_TICKETS);

const stopLoading = combineActions(
  types.GET_TICKETS_FAIL,
  types.GET_TICKETS_SUCCESS,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),
    [types.GET_TICKETS_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      tickets: xmlToJSON(data, 'getMensalidades'),
    }),
    [types.GET_TICKETS_FAIL]: state => ({
      ...state,
      tickets: [],
    }),
  },
  INITIAL_STATE,
);

export default reducer;

