import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import { em, connectStyle, computeProps } from '../../utils';
import styles from './styles';

class NBIcon extends Component {
  render() {
    const { style } = computeProps(this.props);
    const { onPress, size, name, color } = this.props;

    if (onPress) {
      return (
        <TouchableOpacity onPress={() => onPress()} activeOpacity={0.7}>
          <Icon
            {...this.props}
            name={name}
            style={[style, { color: color || '#454F63', fontSize: size || em(1.8) }]}
          />
        </TouchableOpacity>
      );
    }

    return (
      <Icon
        {...this.props}
        name={name}
        style={[style, { color: color || '#454F63', fontSize: size || em(1.8) }]}
      />
    )
  }
}


export default connectStyle('Portal.NBIcon', styles, NBIcon);
