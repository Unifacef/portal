import React, { Component } from 'react';
import { Tabs as NBTabs, Tab } from 'native-base';

import styles from './styles';
import { About, Units } from '..';

class Tabs extends Component {
  render() {
    return (
      <NBTabs
        initialPage={0}
        tabBarUnderlineStyle={styles.tabs}
      >
        <Tab
          textStyle={[styles.textTab, {
            opacity: 0.4,
          }]}
          activeTextStyle={[styles.textTab, {
            opacity: 1,
          }]}
          tabStyle={styles.tab}
          activeTabStyle={styles.tab}
          heading="GERAL"
        >
          <About />
        </Tab>
        <Tab
          textStyle={[styles.textTab, {
            opacity: 0.4,
          }]}
          activeTextStyle={[styles.textTab, {
            opacity: 1,
          }]}
          tabStyle={styles.tab}
          activeTabStyle={styles.tab}
          heading="UNIDADES"
        >
          <Units />
        </Tab>
      </NBTabs>
    )
  }
}

export default Tabs;
