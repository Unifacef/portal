import { handleActions, combineActions } from 'redux-actions';
import { xmlToJSON } from '../../boot/config';
import types from './types';

const INITIAL_STATE = {
  isLoading: false,
  currentSemester: '',
  currentsDisciplines: [],
  isQuestionaryAnswered: true,
};

const beginLoading = combineActions(types.VERIFY_QUESTIONARY);

const stopLoading = combineActions(
  types.VERIFY_QUESTIONARY_FAIL,
  types.VERIFY_QUESTIONARY_SUCCESS,
);

const reducer = handleActions(
  {
    [beginLoading]: state => ({
      ...state,
      isLoading: true,
    }),
    [stopLoading]: state => ({
      ...state,
      isLoading: false,
    }),
    [types.SET_CURRENTS_DISCIPLINES]: (state, { disciplines }) => {
      let currentsDisciplines = [{ name: 'TODAS', id: 0 }];
      disciplines.map((item, index) => {
        return currentsDisciplines.push({
          name: item.nomeDisciplina,
          id: index + 1,
        });
      });
      
      return {
        ...state,
        currentsDisciplines,
      };
    },
    [types.SET_CURRENT_SEMESTER]: (state, { currentSemester }) => ({
      ...state,
      currentSemester,
    }),
    [types.VERIFY_QUESTIONARY_SUCCESS]: (state, { payload: { data } }) => ({
      ...state,
      isQuestionaryAnswered: JSON.parse(xmlToJSON(data, 'avaliacaoRespondida')),
    }),
    [types.VERIFY_QUESTIONARY_FAIL]: (state) => ({
      ...state,
      isQuestionaryAnswered: true,
    }),
  },
  INITIAL_STATE,
);

export default reducer;
